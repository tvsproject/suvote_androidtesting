package uk.ac.surrey.cs.tvs.utils;

/**
 * Defines test parameters for the client common utils testing
 */
public class ClientCommonUtilsTestParameters {

	/**
	 * Groups together related json schema parameters
	 */
	public static class JSONSchema {

		/**
		 * Test JSON schema
		 */
		public static final String SCHEMA = "./testdata/schema.json";

		/**
		 * Invalid test JSON schema. The booth id is marked as an array
		 */
		public static final String INVALID_SCHEMA_1 = "./testdata/invalid_array_boothid_schema.json";

		/**
		 * Invalid test JSON schema. The required values includes the field
		 * "invalid"
		 */
		public static final String INVALID_SCHEMA_2 = "./testdata/invalid_missing_required_schema.json";
	}

	/**
	 * Groups together related json field parameters
	 */
	public static class JSONTestFields {

		/**
		 * Holds a set of valid test permutations
		 */
		public static final String PERMUTATIONS_OBJECT = "[[1,2,3],[4,5,6],[7,8,9]]";

		/**
		 * Empty preferences
		 */
		public static final String EMPTY_PREFERENCES = "[[\" \",\" \",\" \"],[\" \",\" \",\" \"],[\" \",\" \",\" \"]]";

		/**
		 * Holds the above permutations as a string
		 */
		public static final String PERMUTATIONS = "1,2,3:4,5,6:7,8,9:";
		
		/**
		 * Holds the above permutations as a string
		 */
		public static final String EMPTY_PERMUTATIONS = "\" \",\" \",\" \":\" \",\" \",\" \":\" \",\" \",\" \":";
		
		/**
		 * Holds the zero preferences
		 */
		public static final String ZERO_PREFERENCES = ":::";

		/**
		 * Races as a string
		 */
		public static final String RACES = "[{\"id\":\"LA\",\"preferences\":[\"1\",\"2\",\"3\"]},{\"id\":\"LC_ATL\",\"preferences\":[\"4\",\"5\",\"6\"]},{\"id\":\"LC_BTL\",\"preferences\":[\"7\",\"8\",\"9\"]}]";

		/**
		 * Races as a string
		 */
		public static final String RACES_1 = "[{\"id\":\"LA\",\"preferences\":[\" \",\" \",\" \"]},{\"id\":\"LC_ATL\",\"preferences\":[\" \",\" \",\" \"]},{\"id\":\"LC_BTL\",\"preferences\":[\" \",\" \",\" \"]}]";
	
		/**
		 * Holds the empty race preferences
		 */
		public static final String EMPTY_RACE_PREFERENCES = " , , : , , : , , :";
		
		/**
		 * Test booth id
		 */
		public static final String TEST_BOOTH_ID = "TestEVMOne";

		/**
		 * Test serial number
		 */
		public static final String TEST_SERIAL_NO = "TestDeviceOne:101";

		/**
		 * Test start evm sig
		 */
		public static final String TEST_START_EVM_SIG = "TYvc+whKdSxil9MH2Z2T8HnRW0APhYN8vngaYDVDBDD27N85x0RhfQ==";

		/**
		 * Test commit time
		 */
		public static final String TEST_COMMIT_TIME = "1403247600000";

		/**
		 * Test booth sig
		 */
		public static final String TEST_BOOTH_SIG = "YmYT3sTc6f/Qr1uodUWnq5iQfYYzWGQ/yY+aiDtCJP0ihiVk9gN/ZQ==";

		/**
		 * Test serial sig
		 */
		public static final String TEST_SERIAL_SIG = "FnfE2h6EhRdhWkvpLMiEA8eSoDUf0pB2opKOCXyHatNdz34ODGl2Iw==";

		/**
		 * Test district
		 */
		public static final String TEST_DISTRICT = "Northcote";

		/**
		 * Test type
		 */
		public static final String TEST_TYPE = "vote";

		/**
		 * valid test vote message
		 */
		public static final String TEST_VOTE_MESSAGE = "{\""
				+ JSONMessageFields.JSONWBBMessage.SENDER_ID + "\":\""
				+ TEST_BOOTH_ID + "\",\"" + JSONMessageFields.JSONWBBMessage.ID
				+ "\":\"" + TEST_SERIAL_NO + "\",\""
				+ JSONMessageFields.VoteMessage.START_EVM_SIG + "\":\""
				+ TEST_START_EVM_SIG + "\",\""
				+ JSONMessageFields.JSONWBBMessage.COMMIT_TIME + "\":\""
				+ TEST_COMMIT_TIME + "\",\""
				+ JSONMessageFields.JSONWBBMessage.SENDER_SIG + "\":\""
				+ TEST_BOOTH_SIG + "\",\""
				+ JSONMessageFields.VoteMessage.RACES + "\":"
				+ RACES + ",\""
				+ JSONMessageFields.JSONWBBMessage.SERIAL_SIG + "\":\""
				+ TEST_SERIAL_SIG + "\",\""
				+ JSONMessageFields.JSONWBBMessage.DISTRICT + "\":\""
				+ TEST_DISTRICT + "\",\""
				+ JSONMessageFields.JSONWBBMessage.TYPE + "\":\"" + TEST_TYPE
				+ "\",\"" + JSONMessageFields.VoteMessage.INTERNAL_PREFS
				+ "\":\"" + PERMUTATIONS + "\"}";

		/**
		 * invalid test vote message
		 */
		public static final String TEST_INVALID_VOTE_MESSAGE = "{\""
				+ JSONMessageFields.JSONWBBMessage.SENDER_ID + "\":\""
				+ TEST_BOOTH_ID + "\",\"" + JSONMessageFields.JSONWBBMessage.ID
				+ "\":\"" + TEST_SERIAL_NO + "\",\""
				+ JSONMessageFields.VoteMessage.START_EVM_SIG + "\":\""
				+ TEST_START_EVM_SIG + "\",\""
				+ JSONMessageFields.JSONWBBMessage.COMMIT_TIME + "\":\""
				+ TEST_COMMIT_TIME + "\",\""
				+ JSONMessageFields.JSONWBBMessage.SENDER_SIG + "\":\""
				+ TEST_BOOTH_SIG + "\",\""
				+ JSONMessageFields.VoteMessage.RACES + "\":"
				+ RACES + ",\""
				+ JSONMessageFields.JSONWBBMessage.SERIAL_SIG + "\":\""
				+ TEST_SERIAL_SIG + "\",\""
				+ JSONMessageFields.JSONWBBMessage.DISTRICT + "\":\""
				+ TEST_DISTRICT + "\",\""
				+ JSONMessageFields.VoteMessage.INTERNAL_PREFS + "\":\""
				+ PERMUTATIONS + "\"}";
	}

	/**
	 * Groups together json message fields
	 */
	public static class JSONMessageFields {

		/**
		 * generic json wbb message
		 */
		public static class JSONWBBMessage {

			/**
			 * key for serial number
			 */
			public static final String ID = "serialNo";

			/**
			 * Key for type
			 */
			public static final String TYPE = "type";

			/**
			 * Key for wbbid
			 */
			public static final String PEER_ID = "WBBID";

			/**
			 * Key for peer sig
			 */
			public static final String PEER_SIG = "WBBSig";

			/**
			 * Key for commit time
			 */
			public static final String COMMIT_TIME = "commitTime";

			/**
			 * Key for district
			 */
			public static final String DISTRICT = "district";

			/**
			 * Key for sender sig
			 */
			public static final String SENDER_SIG = "boothSig";

			/**
			 * Key for sender id
			 */
			public static final String SENDER_ID = "boothID";

			/**
			 * Key for serial sig
			 */
			public static final String SERIAL_SIG = "serialSig";

		}

		/**
		 * Vote message fields.
		 */
		public static class VoteMessage extends JSONWBBMessage {

			/**
			 * Key for races
			 */
			public static final String RACES = "races";

			/**
			 * Key for lc atl
			 */
			public static final String LC_ATL = "LC_ATL";

			/**
			 * Key for lc btl
			 */
			public static final String LC_BTL = "LC_BTL";

			/**
			 * Key for la
			 */
			public static final String LA = "LA";

			/**
			 * Key for preferences
			 */
			public static final String PREFERENCES = "preferences";

			/**
			 * Key for race id
			 */
			public static final String RACE_ID = "id";

			/**
			 * Key for quote character
			 */
			public static final String QUOTE_CHAR = "\"";

			/**
			 * Key for internal prefs
			 */
			public static final String INTERNAL_PREFS = "_vPrefs";

			/**
			 * Key for start evm sig
			 */
			public static final String START_EVM_SIG = "startEVMSig";
		}
	}
}
