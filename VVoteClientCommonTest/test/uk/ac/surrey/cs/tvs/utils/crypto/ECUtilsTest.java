package uk.ac.surrey.cs.tvs.utils.crypto;

import java.math.BigInteger;
import java.security.SecureRandom;
import org.junit.*;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.robolectric.annotation.Config;
import org.vvote.bouncycastle.jce.spec.ECParameterSpec;
import org.vvote.bouncycastle.math.ec.ECPoint;
import org.vvote.json.JSONObject;

import uk.ac.surrey.cs.tvs.CustomShadowTestRunner;

/**
 * The class <code>ECUtilsTest</code> contains tests for the class
 * <code>{@link ECUtils}</code>.
 */
@RunWith(CustomShadowTestRunner.class)
@Config(manifest = Config.NONE)
public class ECUtilsTest {

	/**
	 * Run the ECUtils() constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testECUtils_1() throws Exception {

		ECUtils result = new ECUtils();
		assertNotNull(result);

		assertNotNull(result.getG());
		assertNotNull(result.getOrderUpperBound());
		assertNotNull(result.getParams());

	}

	/**
	 * Run the ECUtils(String) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testECUtils_2() throws Exception {
		ECUtils result = new ECUtils("P-256");
		assertNotNull(result);

		assertNotNull(result.getG());
		assertNotNull(result.getOrderUpperBound());
		assertNotNull(result.getParams());
	}

	/**
	 * Run the ECUtils(String) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testECUtils_3() throws Exception {
		ECUtils result = new ECUtils(null);
		assertNull(result);
	}

	/**
	 * Run the ECUtils(String) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testECUtils_4() throws Exception {
		ECUtils result = new ECUtils("P-521");
		assertNotNull(result);

		assertNotNull(result.getG());
		assertNotNull(result.getOrderUpperBound());
		assertNotNull(result.getParams());
	}

	/**
	 * Run the JSONObject cipherToJSON(ECPoint[]) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows={ShadowECUtils.class})
	public void testCipherToJSON_1() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint plaintext = fixture.getRandomValue();
		ECPoint publicKey = fixture.getRandomValue();

		ECPoint[] cipher = fixture.encrypt(plaintext, publicKey);

		// test the actual conversion from ECPoint[] to json
		JSONObject result = fixture.cipherToJSON(cipher);

		assertNotNull(result);

		// Test that the JSON content has required fields
		assertTrue(result.has(ECUtils.GR));
		assertTrue(result.has(ECUtils.MYR));

		JSONObject gr = result.getJSONObject(ECUtils.GR);
		assertTrue(gr.has(ECUtils.X_POINT));
		assertTrue(gr.has(ECUtils.Y_POINT));

		JSONObject myr = result.getJSONObject(ECUtils.MYR);
		assertTrue(myr.has(ECUtils.X_POINT));
		assertTrue(myr.has(ECUtils.Y_POINT));

		// Test conversion back to a cipher.
		ECPoint[] converted = fixture.jsonToCipher(result);

		assertEquals(cipher[0], converted[0]);
		assertEquals(cipher[1], converted[1]);
	}

	/**
	 * Run the JSONObject cipherToJSON(ECPoint[]) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testCipherToJSON_2() throws Exception {
		ECUtils fixture = new ECUtils();
		ECPoint[] points = new ECPoint[] {};

		JSONObject result = fixture.cipherToJSON(points);

		assertNotNull(result);
	}

	/**
	 * Run the JSONObject cipherToJSON(ECPoint[]) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testCipherToJSON_3() throws Exception {
		ECUtils fixture = new ECUtils();
		ECPoint[] points = new ECPoint[] { null, null };

		JSONObject result = fixture.cipherToJSON(points);

		assertNotNull(result);
	}

	/**
	 * Run the JSONObject cipherToJSON(ECPoint[]) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testCipherToJSON_4() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint point = fixture.getRandomValue();

		ECPoint[] points = new ECPoint[] { null, point };

		JSONObject result = fixture.cipherToJSON(points);

		assertNotNull(result);
	}

	/**
	 * Run the JSONObject cipherToJSON(ECPoint[]) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testCipherToJSON_5() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint point = fixture.getRandomValue();

		ECPoint[] points = new ECPoint[] { point, null };

		JSONObject result = fixture.cipherToJSON(points);

		assertNotNull(result);
	}

	/**
	 * Run the JSONObject ecPointToJSON(ECPoint) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testEcPointToJSON_1() throws Exception {
		ECUtils fixture = new ECUtils();
		ECPoint plaintext = fixture.getRandomValue();

		JSONObject result = ECUtils.ecPointToJSON(plaintext);

		assertNotNull(result);

		// Test that the JSON content has required fields
		assertTrue(result.has(ECUtils.X_POINT));
		assertTrue(result.has(ECUtils.Y_POINT));

		// Test conversion back to a ec point
		ECPoint converted = fixture.pointFromJSON(result);

		assertEquals(plaintext, converted);
	}

	/**
	 * Run the JSONObject ecPointToJSON(ECPoint) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testEcPointToJSON_2() throws Exception {
		JSONObject result = ECUtils.ecPointToJSON(null);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint getG() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetG_1() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint result = fixture.getG();
		assertNotNull(result);

		assertFalse(result.isInfinity());
		assertFalse(result.isCompressed());
		assertTrue(result.isNormalized());
	}

	/**
	 * Run the BigInteger getOrderUpperBound() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetOrderUpperBound_1() throws Exception {
		ECUtils fixture = new ECUtils();

		BigInteger result = fixture.getOrderUpperBound();

		assertNotNull(result);
	}

	/**
	 * Run the ECParameterSpec getParams() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetParams_1() throws Exception {
		ECUtils fixture = new ECUtils();

		ECParameterSpec result = fixture.getParams();

		assertNotNull(result);
	}

	/**
	 * Run the ECParameterSpec getParams() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetParams_2() throws Exception {
		// default fixture setup
		ECUtils defaultFixture1 = new ECUtils();
		ECParameterSpec defaultSpec1 = defaultFixture1.getParams();
		assertNotNull(defaultSpec1);

		// another default fixture setup
		ECUtils defaultFixture2 = new ECUtils();
		ECParameterSpec defaultSpec2 = defaultFixture2.getParams();
		assertNotNull(defaultSpec2);

		// setup non default fixture with specified curve name
		String curve = "P-521";
		ECUtils p521Fixture = new ECUtils(curve);
		ECParameterSpec p521Result = p521Fixture.getParams();
		assertNotNull(p521Result);

		// test that curves match expected
		assertTrue(defaultSpec1.getCurve().equals(defaultSpec2.getCurve()));
		assertFalse(defaultSpec1.getCurve().equals(p521Result.getCurve()));
		assertFalse(p521Result.getCurve().equals(defaultSpec2.getCurve()));
	}

	/**
	 * Run the BigInteger getRandomInteger(BigInteger,SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetRandomInteger_1() throws Exception {
		ECUtils fixture = new ECUtils();

		BigInteger upper = fixture.getOrderUpperBound();

		BigInteger result = fixture.getRandomInteger(upper, new SecureRandom());

		assertNotNull(result);
		assertTrue(result.compareTo(upper) == -1);

		int bitLength = result.bitLength();
		assertTrue(bitLength <= upper.bitLength());
	}

	/**
	 * Run the BigInteger getRandomInteger(BigInteger,SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetRandomInteger_2() throws Exception {
		ECUtils fixture = new ECUtils();

		BigInteger n = BigInteger.ONE;

		SecureRandom rand = new SecureRandom();

		BigInteger result = fixture.getRandomInteger(n, rand);

		BigInteger zero = BigInteger.ZERO;

		assertNotNull(result);
		assertEquals(0, result.bitLength());
		assertEquals(zero, result);
	}

	/**
	 * Run the BigInteger getRandomInteger(BigInteger,SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetRandomInteger_3() throws Exception {
		ECUtils fixture = new ECUtils();

		BigInteger n = BigInteger.ZERO;

		SecureRandom rand = new SecureRandom();

		BigInteger result = fixture.getRandomInteger(n, rand);

		BigInteger zero = BigInteger.ZERO;

		assertNotNull(result);
		assertEquals(0, result.bitLength());
		assertEquals(zero, result);
	}

	/**
	 * Run the BigInteger getRandomInteger(BigInteger,SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testGetRandomInteger_4() throws Exception {
		ECUtils fixture = new ECUtils();

		BigInteger result = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), null);

		assertNotNull(result);
	}

	/**
	 * Run the BigInteger getRandomInteger(BigInteger,SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testGetRandomInteger_5() throws Exception {
		ECUtils fixture = new ECUtils();

		SecureRandom rand = new SecureRandom();

		BigInteger result = fixture.getRandomInteger(null, rand);

		assertNotNull(result);
	}

	/**
	 * Run the BigInteger getRandomInteger(BigInteger,SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testGetRandomInteger_6() throws Exception {
		ECUtils fixture = new ECUtils();

		BigInteger result = fixture.getRandomInteger(null, null);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint getRandomValue() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetRandomValue_1() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint result = fixture.getRandomValue();

		assertNotNull(result);
		assertEquals(false, result.isInfinity());
	}

	/**
	 * Run the ECPoint getRandomValue(SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetRandomValue_2() throws Exception {
		ECUtils fixture = new ECUtils();
		SecureRandom rand = new SecureRandom();

		ECPoint result = fixture.getRandomValue(rand);

		assertNotNull(result);
		assertEquals(false, result.isInfinity());
	}

	/**
	 * Run the ECPoint getRandomValue(SecureRandom) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testGetRandomValue_3() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint result = fixture.getRandomValue(null);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint[] jsonToCipher(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = org.vvote.json.JSONException.class)
	public void testJsonToCipher_1() throws Exception {
		ECUtils fixture = new ECUtils();
		JSONObject cipher = new JSONObject();

		ECPoint[] result = fixture.jsonToCipher(cipher);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint[] jsonToCipher(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = org.vvote.json.JSONException.class)
	public void testJsonToCipher_2() throws Exception {
		ECUtils fixture = new ECUtils();
		JSONObject cipher = new JSONObject("");

		ECPoint[] result = fixture.jsonToCipher(cipher);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint[] jsonToCipher(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = org.vvote.json.JSONException.class)
	public void testJsonToCipher_3() throws Exception {
		ECUtils fixture = new ECUtils();
		JSONObject cipher = new JSONObject();
		cipher.put(ECUtils.GR, "");
		cipher.put(ECUtils.MYR, "");

		ECPoint[] result = fixture.jsonToCipher(cipher);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint[] jsonToCipher(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = org.vvote.json.JSONException.class)
	public void testJsonToCipher_4() throws Exception {
		ECUtils fixture = new ECUtils();
		JSONObject cipher = new JSONObject();
		cipher.put(ECUtils.GR, (String) null);
		cipher.put(ECUtils.MYR, (String) null);

		ECPoint[] result = fixture.jsonToCipher(cipher);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint[] jsonToCipher(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows={ShadowECUtils.class})
	public void testJsonToCipher_5() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint plaintext = fixture.getRandomValue();
		ECPoint publicKey = fixture.getRandomValue();

		ECPoint[] cipher = fixture.encrypt(plaintext, publicKey);

		// Get a JSONObject cipher
		JSONObject jsonCipher = fixture.cipherToJSON(cipher);

		ECPoint[] result = fixture.jsonToCipher(jsonCipher);

		assertNotNull(result);
		assertEquals(cipher[0], result[0]);
		assertEquals(cipher[1], result[1]);
	}

	/**
	 * Run the ECPoint[] jsonToCipher(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testJsonToCipher_6() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint[] result = fixture.jsonToCipher(null);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint pointFromJSON(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testPointFromJSON_1() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint result = fixture.pointFromJSON(null);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint pointFromJSON(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = org.vvote.json.JSONException.class)
	public void testPointFromJSON_2() throws Exception {
		ECUtils fixture = new ECUtils();
		JSONObject obj = new JSONObject();

		ECPoint result = fixture.pointFromJSON(obj);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint pointFromJSON(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = org.vvote.json.JSONException.class)
	public void testPointFromJSON_3() throws Exception {
		ECUtils fixture = new ECUtils();
		JSONObject obj = new JSONObject("");

		ECPoint result = fixture.pointFromJSON(obj);

		assertNotNull(result);
	}

	/**
	 * Run the ECPoint pointFromJSON(JSONObject) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testPointFromJSON_4() throws Exception {
		ECUtils fixture = new ECUtils();

		ECPoint plaintext = fixture.getRandomValue();

		JSONObject jsonPoint = ECUtils.ecPointToJSON(plaintext);

		assertNotNull(jsonPoint);

		ECPoint result = fixture.pointFromJSON(jsonPoint);

		assertNotNull(result);
		assertEquals(plaintext, result);
	}
}