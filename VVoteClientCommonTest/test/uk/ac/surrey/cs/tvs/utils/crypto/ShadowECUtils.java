package uk.ac.surrey.cs.tvs.utils.crypto;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;
import org.robolectric.annotation.RealObject;
import org.vvote.bouncycastle.math.ec.ECPoint;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * Shadow Implementation for <code>ECUtils</code>
 */
@Implements(ECUtils.class)
public class ShadowECUtils {

	/**
	 * Provides access to the real ECUtils class
	 */
	@RealObject
	private ECUtils realECUtils;

	/**
	 * Provides a non native implementation of encrypt
	 * 
	 * @param plaintext
	 * @param publicKey
	 * @return
	 */
	@Implementation
	public ECPoint[] encrypt(ECPoint plaintext, ECPoint publicKey) {
		// Generate some new randomness
		BigInteger randomness = new BigInteger(this.realECUtils.getParams()
				.getN().bitLength(), new SecureRandom());

		return this.encrypt(plaintext, publicKey, randomness);
	}

	/**
	 * Provides a non native implementation of encrypt
	 * 
	 * @param plaintext
	 * @param publicKey
	 * @param randomness 
	 * @return
	 */
	@Implementation
	public ECPoint[] encrypt(ECPoint plaintext, ECPoint publicKey,
			BigInteger randomness) {
		ECPoint gr = this.realECUtils.getParams().getG().multiply(randomness);
		ECPoint myr = publicKey.multiply(randomness).add(plaintext);

		return new ECPoint[] { gr, myr };
	}
}
