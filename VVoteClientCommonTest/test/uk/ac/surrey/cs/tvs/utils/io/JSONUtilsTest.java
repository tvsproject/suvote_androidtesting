package uk.ac.surrey.cs.tvs.utils.io;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.mozilla.javascript.RhinoException;

import uk.ac.surrey.cs.tvs.utils.ClientCommonUtilsTestParameters;
import static org.junit.Assert.*;

/**
 * The class <code>JSONUtilsTest</code> contains tests for the class <code>{@link JSONUtils}</code>.
 */
public class JSONUtilsTest {
	/**
	 * Run the JSONUtils() constructor test.
	 * @throws Exception 
	 */
	@Test
	public void testJSONUtils_1()
		throws Exception {
		JSONUtils result = new JSONUtils();
		assertNotNull(result);
	}

	/**
	 * Run the String loadSchema(String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException.class)
	public void testLoadSchema_1()
		throws Exception {
		String schemaPath = "";

		String result = JSONUtils.loadSchema(schemaPath);
		assertNotNull(result);
	}

	/**
	 * Run the String loadSchema(String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testLoadSchema_2()
		throws Exception {
		String result = JSONUtils.loadSchema(null);

		assertNotNull(result);
	}
	
	/**
	 * Run the String loadSchema(String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testLoadSchema_3()
		throws Exception {
		String result = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.SCHEMA);

		assertNotNull(result);
	}
	
	/**
	 * Run the String loadSchema(String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testLoadSchema_4()
		throws Exception {
		String result = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_1);

		assertNotNull(result);
	}
	
	/**
	 * Run the String loadSchema(String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testLoadSchema_5()
		throws Exception {
		String result = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_2);

		assertNotNull(result);
	}

	/**
	 * Run the String permutationsToString(JSONArray) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testPermutationsToString_1()
		throws Exception {
		JSONArray perms = new JSONArray();

		String result = JSONUtils.permutationsToString(perms);

		assertNotNull(result);
		assertEquals("", result);
	}

	/**
	 * Run the String permutationsToString(JSONArray) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testPermutationsToString_2()
		throws Exception {
		JSONArray perms = new JSONArray(ClientCommonUtilsTestParameters.JSONTestFields.PERMUTATIONS_OBJECT);

		String result = JSONUtils.permutationsToString(perms);

		assertNotNull(result);
		assertEquals(ClientCommonUtilsTestParameters.JSONTestFields.PERMUTATIONS, result);
	}
	
	/**
	 * Run the String permutationsToString(JSONArray) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testPermutationsToString_3()
		throws Exception {
		JSONArray perms = new JSONArray(ClientCommonUtilsTestParameters.JSONTestFields.EMPTY_PREFERENCES);

		String result = JSONUtils.permutationsToString(perms);

		assertNotNull(result);
		assertEquals(ClientCommonUtilsTestParameters.JSONTestFields.EMPTY_PERMUTATIONS, result);
	}

	/**
	 * Run the String preferencesToString(JSONArray) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testPreferencesToString_1()
		throws Exception {
	    JSONArray races = new JSONArray(ClientCommonUtilsTestParameters.JSONTestFields.RACES);

	    String result = JSONUtils.preferencesToString(races);
	    assertNotNull(result);

	    assertEquals(ClientCommonUtilsTestParameters.JSONTestFields.PERMUTATIONS, result);
	}

	/**
	 * Run the String preferencesToString(JSONArray) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testPreferencesToString_2()
		throws Exception {
		JSONArray races = new JSONArray();

		String result = JSONUtils.preferencesToString(races);

		assertNotNull(result);
	}

	/**
	 * Run the String preferencesToString(JSONArray) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testPreferencesToString_3()
		throws Exception {
		JSONArray races = new JSONArray("[]");

		String result = JSONUtils.preferencesToString(races);

		assertNotNull(result);
	}
	
	/**
	 * Run the String preferencesToString(JSONArray) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testPreferencesToString_4()
		throws Exception {
	    JSONArray races = new JSONArray(ClientCommonUtilsTestParameters.JSONTestFields.RACES_1);

	    String result = JSONUtils.preferencesToString(races);
	    assertNotNull(result);

	    assertEquals(ClientCommonUtilsTestParameters.JSONTestFields.EMPTY_RACE_PREFERENCES, result);
	}

	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = RhinoException.class)
	public void testValidateSchema_1()
		throws Exception {
		String schema = "";
		String json = "";

		boolean result = JSONUtils.validateSchema(schema, json);
		assertFalse(result);
	}

	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = RhinoException.class)
	public void testValidateSchema_2()
		throws Exception {

		boolean result = JSONUtils.validateSchema(null, null);

		assertFalse(result);
	}

	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = RhinoException.class)
	public void testValidateSchema_3()
		throws Exception {
		String json = "";

		boolean result = JSONUtils.validateSchema(null, json);
		
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = RhinoException.class)
	public void testValidateSchema_4()
		throws Exception {
		String schema = "";

		boolean result = JSONUtils.validateSchema(schema, null);
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testValidateSchema_5()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.SCHEMA);

		boolean result = JSONUtils.validateSchema(schema, null);
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testValidateSchema_6()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_1);

		boolean result = JSONUtils.validateSchema(schema, null);
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testValidateSchema_7()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_2);

		boolean result = JSONUtils.validateSchema(schema, null);
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = RhinoException.class)
	public void testValidateSchema_8()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.SCHEMA);

		boolean result = JSONUtils.validateSchema(schema, "");
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = RhinoException.class)
	public void testValidateSchema_9()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_1);

		boolean result = JSONUtils.validateSchema(schema, "");
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test(expected = RhinoException.class)
	public void testValidateSchema_10()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_2);

		boolean result = JSONUtils.validateSchema(schema, "");
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testValidateSchema_11()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.SCHEMA);		
		
		// create valid vote message
		JSONObject voteMessage = new JSONObject(ClientCommonUtilsTestParameters.JSONTestFields.TEST_VOTE_MESSAGE);
		
		boolean result = JSONUtils.validateSchema(schema, voteMessage.toString());
		assertTrue(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testValidateSchema_12()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.SCHEMA);

		// create invalid vote message
		JSONObject voteMessage = new JSONObject(ClientCommonUtilsTestParameters.JSONTestFields.TEST_INVALID_VOTE_MESSAGE);
		
		boolean result = JSONUtils.validateSchema(schema, voteMessage.toString());
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testValidateSchema_13()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_1);

		// create invalid vote message
		JSONObject voteMessage = new JSONObject(ClientCommonUtilsTestParameters.JSONTestFields.TEST_VOTE_MESSAGE);
		
		boolean result = JSONUtils.validateSchema(schema, voteMessage.toString());
		assertFalse(result);
	}
	
	/**
	 * Run the boolean validateSchema(String,String) method test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testValidateSchema_14()
		throws Exception {
		String schema = JSONUtils.loadSchema(ClientCommonUtilsTestParameters.JSONSchema.INVALID_SCHEMA_2);

		// create invalid vote message
		JSONObject voteMessage = new JSONObject(ClientCommonUtilsTestParameters.JSONTestFields.TEST_VOTE_MESSAGE);
		
		boolean result = JSONUtils.validateSchema(schema, voteMessage.toString());
		assertFalse(result);
	}
}