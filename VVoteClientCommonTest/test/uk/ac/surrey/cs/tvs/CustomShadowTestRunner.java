package uk.ac.surrey.cs.tvs;

import java.io.IOException;
import java.util.Properties;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.bytecode.ClassInfo;
import org.robolectric.bytecode.Setup;

/**
 * CustomShadowTestRunner to provide the ability to locate the classes which are
 * to be shadowed. The classes for shadowing are read in from file
 * 
 */
public class CustomShadowTestRunner extends RobolectricTestRunner {

	/**
	 * Constructor for the CustomShadowTestRunner
	 * 
	 * @param testClass
	 * @throws InitializationError
	 */
	public CustomShadowTestRunner(Class<?> testClass)
			throws InitializationError {
		super(testClass);
	}

	@Override
	public Setup createSetup() {
		return new CustomShadowSetup();
	}
}

/**
 * Provides a custom setup for the CustomShadowTestRunner
 */
class CustomShadowSetup extends Setup {

	/**
	 * Properties object to hold list of shadows from the shadows.properties
	 * file
	 */
	private Properties shadows = new Properties();

	/**
	 * Constructor for the custom setup
	 */
	public CustomShadowSetup() {
		super();
		try {
			// Load shadows from file
			this.shadows.load(this.getClass().getClassLoader()
					.getResourceAsStream("shadows.properties"));
		} catch (IOException e) {
			System.err.println("shadows.properties not found!");
		}
	}

	@Override
	public boolean shouldInstrument(ClassInfo info) {
		// Check shadows properties and instrument if listed
		boolean instrument = (super.shouldInstrument(info) || this.shadows
				.containsKey(info.getName()));
		return instrument;
	}
}
