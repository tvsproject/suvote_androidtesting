package uk.ac.surrey.cs.tvs.vvote.webcam;

import android.widget.Button;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.util.ActivityController;

import uk.ac.surrey.cs.tvs.android.client.R;
import uk.ac.surrey.cs.tvs.vvote.ClientCommonVVoteTestParameters;

/**
 * The class <code>WebCamCallibrateTest</code> contains tests for the class
 * <code>{@link WebCamCallibrate}</code>.
 */
@RunWith(RobolectricTestRunner.class)
public class WebCamCallibrateTest {

	/**
	 * Controller used for getting robolectric activities for
	 * <code>WebCamCalibrate</code>
	 */
	private final static ActivityController<WebCamCallibrate> controller = Robolectric
			.buildActivity(WebCamCallibrate.class);

	/**
	 * Test to ensure the activity can be created successfully
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetupConfiguration() throws Exception {
		WebCamCallibrate activity = Robolectric
				.buildActivity(WebCamCallibrate.class).create().get();
		String hello = activity.getString(R.string.hello_world);
		assertTrue(hello.equals(ClientCommonVVoteTestParameters.TEST_MESSAGE));
	}

	/**
	 * Run the void onClick(View) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testOnClick_1() throws Exception {
		WebCamCallibrate activity = controller.create().get();
		Button view = (Button) activity.findViewById(R.id.exitButton);

		assertNotNull(view);

		assertFalse(activity.isFinishing());

		assertTrue(view.performClick());

		assertTrue(activity.isFinishing());
	}

	/**
	 * Run the void onCreate(Bundle) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testOnCreate_1() throws Exception {
		WebCamCallibrate activity = controller.create().get();
		Button view = (Button) activity.findViewById(R.id.exitButton);

		assertNotNull(view);

		assertEquals(R.id.exitButton, view.getId());
	}
}
