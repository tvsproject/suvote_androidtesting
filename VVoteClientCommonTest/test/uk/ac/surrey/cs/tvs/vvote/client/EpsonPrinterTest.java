package uk.ac.surrey.cs.tvs.vvote.client;

import java.lang.reflect.Field;

import org.junit.*;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import uk.ac.surrey.cs.tvs.CustomShadowTestRunner;
import uk.ac.surrey.cs.tvs.vvote.ClientCommonVVoteTestParameters;
import android.graphics.Bitmap;
import android.content.Context;
import static org.junit.Assert.*;

/**
 * The class <code>EpsonPrinterTest</code> contains tests for the class
 * <code>{@link EpsonPrinter}</code>.
 */
@RunWith(CustomShadowTestRunner.class)
@Config(manifest = Config.NONE)
public class EpsonPrinterTest {

	/**
	 * Run the EpsonPrinter(ConnectionType,Context) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testEpsonPrinter_1() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.NETWORK;
		Context ctx = Robolectric.application;

		EpsonPrinter result = new EpsonPrinter(type, ctx);

		assertNotNull(result);
	}

	/**
	 * Run the EpsonPrinter(ConnectionType,Context) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class })
	public void testEpsonPrinter_2() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter result = new EpsonPrinter(type, ctx);

		assertNotNull(result);
	}

	/**
	 * Run the void close() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class })
	public void testClose_1() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter fixture = new EpsonPrinter(type, ctx);

		assertNotNull(fixture);

		Field field = fixture.getClass().getDeclaredField(
				ClientCommonVVoteTestParameters.EPSON_THERMAL_PRINTER_FIELD);
		field.setAccessible(true);
		EpsonThermalPrinter etp = (EpsonThermalPrinter) field.get(fixture);

		// get the usb printer
		ShadowEpsonUSBPrinter shadowEtp = Robolectric.shadowOf_(etp);

		// check that the method has not been called
		assertFalse(shadowEtp.hasCloseBeenCalled());

		// call close
		fixture.close();

		// check that the method has been called
		assertTrue(shadowEtp.hasCloseBeenCalled());
	}

	/**
	 * Run the OutputStream getOutputStream() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class,
			ShadowBulkUSBBufferedOutputStream.class,
			ShadowByteArrayOutputStream.class })
	public void testGetOutputStream_1() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter fixture = new EpsonPrinter(type, ctx);

		assertNotNull(fixture);

		Field field = fixture.getClass().getDeclaredField(
				ClientCommonVVoteTestParameters.EPSON_THERMAL_PRINTER_FIELD);
		field.setAccessible(true);
		EpsonThermalPrinter etp = (EpsonThermalPrinter) field.get(fixture);

		// get the usb printer
		ShadowEpsonUSBPrinter shadowEtp = Robolectric.shadowOf_(etp);

		// check that the method has not been called
		assertFalse(shadowEtp.hasGetOutputStreamCalled());

		// call getOutputStream
		fixture.getOutputStream();

		// check that the method has been called
		assertTrue(shadowEtp.hasGetOutputStreamCalled());
	}

	/**
	 * Run the boolean isPrinterReady() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class })
	public void testIsPrinterReady_1() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter fixture = new EpsonPrinter(type, ctx);

		assertNotNull(fixture);

		Field field = fixture.getClass().getDeclaredField(
				ClientCommonVVoteTestParameters.EPSON_THERMAL_PRINTER_FIELD);
		field.setAccessible(true);
		EpsonThermalPrinter etp = (EpsonThermalPrinter) field.get(fixture);

		// get the usb printer
		ShadowEpsonUSBPrinter shadowEtp = Robolectric.shadowOf_(etp);

		// check that the method has not been called
		assertFalse(shadowEtp.hasIsReadyBeenCalled());

		// check that the printer is not ready
		assertFalse(fixture.isPrinterReady());

		// check that the method has been called
		assertTrue(shadowEtp.hasIsReadyBeenCalled());

		// set the printer to ready
		shadowEtp.setIsReady();

		// check that the printer is ready
		assertTrue(fixture.isPrinterReady());
	}

	/**
	 * Run the void printImage(Bitmap) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testPrintImage_1() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter fixture = new EpsonPrinter(type, ctx);

		assertNotNull(fixture);

		fixture.printImage(null);
	}

	/**
	 * Run the void printImage(Bitmap) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class,
			ShadowBulkUSBBufferedOutputStream.class,
			ShadowByteArrayOutputStream.class, ShadowOutputStream.class })
	public void testPrintImage_2() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter fixture = new EpsonPrinter(type, ctx);

		assertNotNull(fixture);

		// create test bitmap
		Bitmap bitmap = Bitmap.createBitmap(
				ClientCommonVVoteTestParameters.IMAGE_WIDTH,
				ClientCommonVVoteTestParameters.IMAGE_HEIGHT,
				ClientCommonVVoteTestParameters.IMAGE_CONFIG);

		// get the output stream
		ShadowBulkUSBBufferedOutputStream out = Robolectric.shadowOf_(fixture
				.getOutputStream());

		// check that none of its methods have been called
		assertFalse(out.hasWriteBeenCalled());
		assertFalse(out.hasWriteWithOffsetAndLengthCalledBeenCalled());
		assertFalse(out.hasFlushBeenCalled());

		// call print image
		fixture.printImage(bitmap);

		// check that its methods have been called
		assertFalse(out.hasWriteBeenCalled());
		assertTrue(out.hasWriteWithOffsetAndLengthCalledBeenCalled());
		assertTrue(out.hasFlushBeenCalled());
	}

	/**
	 * Run the void printImage(Bitmap) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class,
			ShadowBulkUSBBufferedOutputStream.class,
			ShadowByteArrayOutputStream.class, ShadowOutputStream.class })
	public void testPrintImage_3() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter fixture = new EpsonPrinter(type, ctx);

		assertNotNull(fixture);

		// create second test bitmap with 0 dimensions
		Bitmap bitmap = Bitmap.createBitmap(
				ClientCommonVVoteTestParameters.IMAGE_WIDTH_1,
				ClientCommonVVoteTestParameters.IMAGE_HEIGHT_1,
				ClientCommonVVoteTestParameters.IMAGE_CONFIG);

		ShadowBulkUSBBufferedOutputStream out = Robolectric.shadowOf_(fixture
				.getOutputStream());

		assertFalse(out.hasWriteBeenCalled());
		assertFalse(out.hasWriteWithOffsetAndLengthCalledBeenCalled());
		assertFalse(out.hasFlushBeenCalled());

		fixture.printImage(bitmap);

		assertFalse(out.hasWriteBeenCalled());
		assertTrue(out.hasWriteWithOffsetAndLengthCalledBeenCalled());
		assertTrue(out.hasFlushBeenCalled());
	}

	/**
	 * Run the void setBuffer(int) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class })
	public void testSetBuffer_1() throws Exception {
		EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
		Context ctx = Robolectric.application;

		EpsonPrinter fixture = new EpsonPrinter(type, ctx);

		assertNotNull(fixture);

		Field field = fixture.getClass().getDeclaredField(
				ClientCommonVVoteTestParameters.EPSON_THERMAL_PRINTER_FIELD);
		field.setAccessible(true);
		EpsonThermalPrinter etp = (EpsonThermalPrinter) field.get(fixture);

		ShadowEpsonUSBPrinter shadowEtp = Robolectric.shadowOf_(etp);

		assertFalse(shadowEtp.hasSetBufferBeenCalled());

		fixture.setBuffer(0);

		assertTrue(shadowEtp.hasSetBufferBeenCalled());
	}
}