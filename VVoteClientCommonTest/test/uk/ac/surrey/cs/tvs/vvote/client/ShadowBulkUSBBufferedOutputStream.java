package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.IOException;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;

/**
 * Shadow Implementation for <code>BulkUSBBufferedOutputStream</code>
 */
@Implements(value = BulkUSBBufferedOutputStream.class, inheritImplementationMethods = true)
public class ShadowBulkUSBBufferedOutputStream extends
		ShadowByteArrayOutputStream {

	/**
	 * Flag for whether flush has been called
	 */
	private boolean flushCalled = false;

	/**
	 * Shadow Implementation for BulkUSBBufferedOutputStream.flush
	 * 
	 * @throws IOException
	 */
	@Implementation
	public synchronized void flush() throws IOException {
		this.flushCalled = true;
	}

	/**
	 * Getter for whether BulkUSBBufferedOutputStream.flush has been called
	 * 
	 * @return flushCalled
	 */
	public boolean hasFlushBeenCalled() {
		return this.flushCalled;
	}
}
