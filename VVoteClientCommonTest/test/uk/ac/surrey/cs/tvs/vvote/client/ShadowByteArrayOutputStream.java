package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.ByteArrayOutputStream;

import org.robolectric.annotation.Implements;

/**
 * Shadow Implementation for <code>ByteArrayOutputStream</code>
 */
@Implements(value = ByteArrayOutputStream.class, inheritImplementationMethods = true)
public class ShadowByteArrayOutputStream extends ShadowOutputStream {

	/**
	 * Constructor used for debugging
	 */
	public ShadowByteArrayOutputStream() {
		super();
	}
}
