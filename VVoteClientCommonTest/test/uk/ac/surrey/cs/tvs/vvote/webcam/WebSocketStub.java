package uk.ac.surrey.cs.tvs.vvote.webcam;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;

/**
 * Stub WebSocket
 */
public class WebSocketStub extends WebSocket {

	/**
	 * Reference to the sent message
	 */
	private String sentMessage = null;

	@Override
	public void close(int arg0) {
		// stub
	}

	@Override
	protected void close(InvalidDataException arg0) {
		// stub
	}

	@Override
	public void close(int arg0, String arg1) {
		// stub
	}

	@Override
	public Draft getDraft() {
		return null;
	}

	@Override
	public InetSocketAddress getLocalSocketAddress() {
		return null;
	}

	@Override
	public int getReadyState() {
		return 0;
	}

	@Override
	public InetSocketAddress getRemoteSocketAddress() {
		return null;
	}

	@Override
	public boolean hasBufferedData() {
		return false;
	}

	@Override
	public boolean isClosed() {
		return false;
	}

	@Override
	public boolean isClosing() {
		return false;
	}

	@Override
	public boolean isConnecting() {
		return false;
	}

	@Override
	public boolean isOpen() {
		return false;
	}

	@Override
	public void send(String message) throws NotYetConnectedException {
		this.sentMessage = message;
	}

	@Override
	public void send(ByteBuffer arg0) throws IllegalArgumentException,
			NotYetConnectedException {
		// stub
	}

	@Override
	public void send(byte[] arg0) throws IllegalArgumentException,
			NotYetConnectedException {
		// stub
	}

	@Override
	public void sendFrame(Framedata arg0) {
		// stub
	}

	@Override
	public void startHandshake(ClientHandshakeBuilder arg0)
			throws InvalidHandshakeException {
		// stub
	}

	/**
	 * Simple getter for the sent message
	 * 
	 * @return sentMessage
	 */
	public String getSentMessage() {
		return this.sentMessage;
	}
}
