package uk.ac.surrey.cs.tvs.vvote.client.webcam;

import org.junit.*;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import uk.ac.surrey.cs.tvs.vvote.ClientCommonVVoteTestParameters;

import static org.junit.Assert.*;

/**
 * The class <code>CameraNotAttachedExceptionTest</code> contains tests for the
 * class <code>{@link CameraNotAttachedException}</code>.
 */
@RunWith(RobolectricTestRunner.class)
public class CameraNotAttachedExceptionTest {
	/**
	 * Run the CameraNotAttachedException() constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCameraNotAttachedException_1() throws Exception {

		CameraNotAttachedException result = new CameraNotAttachedException();

		assertNotNull(result);
		assertEquals(null, result.getCause());
		assertEquals(
				"uk.ac.surrey.cs.tvs.vvote.client.webcam.CameraNotAttachedException",
				result.toString());
		assertEquals(null, result.getMessage());
		assertEquals(null, result.getLocalizedMessage());
	}

	/**
	 * Run the CameraNotAttachedException(String) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCameraNotAttachedException_2() throws Exception {
		String message = ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE;

		CameraNotAttachedException result = new CameraNotAttachedException(
				message);

		assertNotNull(result);
		assertEquals(null, result.getCause());
		assertEquals(
				"uk.ac.surrey.cs.tvs.vvote.client.webcam.CameraNotAttachedException: "
						+ ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE,
				result.toString());
		assertEquals(ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE,
				result.getMessage());
		assertEquals(ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE,
				result.getLocalizedMessage());
	}

	/**
	 * Run the CameraNotAttachedException(Throwable) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCameraNotAttachedException_3() throws Exception {
		Throwable cause = new Throwable();

		CameraNotAttachedException result = new CameraNotAttachedException(
				cause);

		assertNotNull(result);
		assertEquals(
				"uk.ac.surrey.cs.tvs.vvote.client.webcam.CameraNotAttachedException: java.lang.Throwable",
				result.toString());
		assertEquals("java.lang.Throwable", result.getMessage());
		assertEquals("java.lang.Throwable", result.getLocalizedMessage());
	}

	/**
	 * Run the CameraNotAttachedException(String,Throwable) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCameraNotAttachedException_4() throws Exception {
		String message = ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE;
		Throwable cause = new Throwable();

		CameraNotAttachedException result = new CameraNotAttachedException(
				message, cause);

		assertNotNull(result);
		assertEquals(
				"uk.ac.surrey.cs.tvs.vvote.client.webcam.CameraNotAttachedException: "
						+ ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE,
				result.toString());
		assertEquals(ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE,
				result.getMessage());
		assertEquals(ClientCommonVVoteTestParameters.EXCEPTION_MESSAGE,
				result.getLocalizedMessage());
	}
}