package uk.ac.surrey.cs.tvs.vvote.client;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.*;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response;

import uk.ac.surrey.cs.tvs.CustomShadowTestRunner;
import uk.ac.surrey.cs.tvs.vvote.ClientCommonVVoteTestParameters;
import uk.ac.surrey.cs.tvs.vvote.VVoteService;
import static org.junit.Assert.*;

/**
 * The class <code>PrintServletTest</code> contains tests for the class
 * <code>{@link PrintServlet}</code>.
 */
@RunWith(CustomShadowTestRunner.class)
@Config(manifest = Config.NONE)
public class PrintServletTest {
	/**
	 * Run the PrintServlet(VVoteServiceInterface) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testPrintServlet_1() throws Exception {
		VVoteServiceInterface service = null;

		PrintServlet result = new PrintServlet(service);
		assertNotNull(result);
	}

	/**
	 * Run the PrintServlet(VVoteServiceInterface) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class })
	public void testPrintServlet_2() throws Exception {
		VVoteServiceInterface service = Robolectric
				.buildService(VVoteService.class).create().get();

		PrintServlet result = new PrintServlet(service);
		assertNotNull(result);
	}

	/**
	 * Run the Response runServlet(String,Method,Map<String, String>,Map<String,
	 * String>,Map<String, String>,File) test
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testRunServlet_1() throws Exception {
		VVoteServiceInterface service = Robolectric
				.buildService(VVoteService.class).create().get();

		PrintServlet fixture = new PrintServlet(service);
		assertNotNull(fixture);

		// run with null inputs
		fixture.runServlet(null, null, null, null, null, null);
	}

	/**
	 * Run the Response runServlet(String,Method,Map<String, String>,Map<String,
	 * String>,Map<String, String>,File) test
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class })
	public void testRunServlet_2() throws Exception {
		VVoteServiceInterface service = Robolectric
				.buildService(VVoteService.class).create().get();

		PrintServlet fixture = new PrintServlet(service);
		assertNotNull(fixture);

		// provide non null map with no elements
		Response response = fixture.runServlet(null, null, null,
				new HashMap<String, String>(), null, null);
		assertTrue(response.getStatus().equals(Response.Status.BAD_REQUEST));
		assertTrue(response.getMimeType().equals(NanoHTTPD.MIME_PLAINTEXT));
	}

	/**
	 * Run the Response runServlet(String,Method,Map<String, String>,Map<String,
	 * String>,Map<String, String>,File) test
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class })
	public void testRunServlet_3() throws Exception {
		VVoteServiceInterface service = Robolectric
				.buildService(VVoteService.class).create().get();

		PrintServlet fixture = new PrintServlet(service);
		assertNotNull(fixture);

		Map<String, String> params = new HashMap<String, String>();
		params.put(ClientCommonVVoteTestParameters.IMAGE_CONTENT_KEY,
				ClientCommonVVoteTestParameters.IMAGE_CONTENT_VALUE);

		// provide valid map
		Response response = fixture.runServlet(null, null, null, params, null,
				null);
		assertTrue(response.getStatus().equals(Response.Status.INTERNAL_ERROR));
		assertTrue(response.getMimeType().equals(NanoHTTPD.MIME_PLAINTEXT));
	}

	/**
	 * Run the Response runServlet(String,Method,Map<String, String>,Map<String,
	 * String>,Map<String, String>,File) test
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class,
			ShadowBulkUSBBufferedOutputStream.class,
			ShadowByteArrayOutputStream.class })
	public void testRunServlet_4() throws Exception {
		VVoteServiceInterface service = Robolectric
				.buildService(VVoteService.class).create().get();

		PrintServlet fixture = new PrintServlet(service);
		assertNotNull(fixture);

		// valid map
		Map<String, String> params = new HashMap<String, String>();
		params.put(ClientCommonVVoteTestParameters.IMAGE_CONTENT_KEY,
				ClientCommonVVoteTestParameters.IMAGE_CONTENT_VALUE);

		Field field = service
				.getEpsonPrinter()
				.getClass()
				.getDeclaredField(
						ClientCommonVVoteTestParameters.EPSON_THERMAL_PRINTER_FIELD);
		field.setAccessible(true);
		EpsonThermalPrinter etp = (EpsonThermalPrinter) field.get(service
				.getEpsonPrinter());

		// get the usb printer
		ShadowEpsonUSBPrinter shadowEtp = Robolectric.shadowOf_(etp);

		// check if printer is ready
		assertFalse(service.getEpsonPrinter().isPrinterReady());

		// set that the printer is ready
		shadowEtp.setIsReady();

		// check if printer is ready
		assertTrue(service.getEpsonPrinter().isPrinterReady());

		Response response = fixture.runServlet(null, null, null, params, null,
				null);
		assertTrue(response.getStatus().equals(Response.Status.OK));
		assertTrue(response.getMimeType().equals(NanoHTTPD.MIME_PLAINTEXT));
	}

	/**
	 * Run the Response runServlet(String,Method,Map<String, String>,Map<String,
	 * String>,Map<String, String>,File) test
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowEpsonUSBPrinter.class,
			ShadowBulkUSBBufferedOutputStream.class,
			ShadowByteArrayOutputStream.class })
	public void testRunServlet_5() throws Exception {
		VVoteServiceInterface service = Robolectric
				.buildService(VVoteService.class).create().get();

		// providing non standard config file which will change the buffer size
		((VVoteService) service)
				.setConfigName(ClientCommonVVoteTestParameters.SERVICE_CONF);

		PrintServlet fixture = new PrintServlet(service);
		assertNotNull(fixture);

		// valid map
		Map<String, String> params = new HashMap<String, String>();
		params.put(ClientCommonVVoteTestParameters.IMAGE_CONTENT_KEY,
				ClientCommonVVoteTestParameters.IMAGE_CONTENT_VALUE);

		Field field = service
				.getEpsonPrinter()
				.getClass()
				.getDeclaredField(
						ClientCommonVVoteTestParameters.EPSON_THERMAL_PRINTER_FIELD);
		field.setAccessible(true);
		EpsonThermalPrinter etp = (EpsonThermalPrinter) field.get(service
				.getEpsonPrinter());

		// get the usb printer
		ShadowEpsonUSBPrinter shadowEtp = Robolectric.shadowOf_(etp);

		// check buffer size is not 5
		assertTrue(shadowEtp.getCurrentBufferSize() != ClientCommonVVoteTestParameters.SERVICE_CONF_NEW_BUFFER_SIZE);

		// set that the printer is ready
		shadowEtp.setIsReady();

		assertTrue(service.getEpsonPrinter().isPrinterReady());

		Response response = fixture.runServlet(null, null, null, params, null,
				null);
		assertTrue(response.getStatus().equals(Response.Status.OK));
		assertTrue(response.getMimeType().equals(NanoHTTPD.MIME_PLAINTEXT));

		// get shadow usb printer
		shadowEtp = Robolectric.shadowOf_(etp);

		// check that its buffer size has been changed
		assertTrue(shadowEtp.getCurrentBufferSize() == ClientCommonVVoteTestParameters.SERVICE_CONF_NEW_BUFFER_SIZE);
	}
}