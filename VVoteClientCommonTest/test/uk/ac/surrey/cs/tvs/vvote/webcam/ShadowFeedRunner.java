package uk.ac.surrey.cs.tvs.vvote.webcam;

import org.java_websocket.WebSocket;
import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;

import uk.ac.surrey.cs.tvs.vvote.client.VVoteServiceInterface;

/**
 * Shadow Implementation for <code>FeedRunner</code>
 */
@Implements(FeedRunner.class)
public class ShadowFeedRunner {

	/**
	 * Underlying Service
	 */
	private VVoteServiceInterface service = null;

	/**
	 * Underlying web socket
	 */
	private WebSocket socket = null;

	/**
	 * Has start been called
	 */
	private boolean startCalled = false;

	/**
	 * has stop been called
	 */
	private boolean stopCalled = false;

	/**
	 * Constructor for ShadowFeedRunner
	 * 
	 * @param service
	 * @param ws
	 */
	public void __constructor__(VVoteServiceInterface service, WebSocket ws) {
		this.service = service;
		this.socket = ws;
	}

	/**
	 * Shadow Implementation for FeedRunner.start
	 * 
	 * @return
	 */
	@Implementation
	public boolean start() {
		this.startCalled = true;
		return this.startCalled;
	}

	/**
	 * getter for whether start has been called
	 * 
	 * @return startCalled
	 */
	public boolean hasStartBeenCalled() {
		return this.startCalled;
	}

	/**
	 * Shadow Implementation for FeedRunner.stop
	 * 
	 * @return
	 */
	@Implementation
	public boolean stop() {
		this.stopCalled = true;
		return this.stopCalled;
	}

	/**
	 * Getter for whether stop has been called
	 * 
	 * @return stopCalled
	 */
	public boolean hasStopBeenCalled() {
		return this.stopCalled;
	}

	/**
	 * Getter for the underlying service
	 * 
	 * @return service
	 */
	public VVoteServiceInterface getService() {
		return this.service;
	}

	/**
	 * Getter for the underlying socket
	 * 
	 * @return socket
	 */
	public WebSocket getSocket() {
		return this.socket;
	}
}
