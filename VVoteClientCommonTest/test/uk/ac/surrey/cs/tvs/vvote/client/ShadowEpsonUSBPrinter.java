package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.OutputStream;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;

import android.content.Context;

/**
 * Shadow Implementation for <code>EpsonUSBPrinter</code>
 */
@Implements(EpsonUSBPrinter.class)
public class ShadowEpsonUSBPrinter {

	/**
	 * Provides context
	 */
	private Context context;

	/**
	 * The output stream
	 */
	private BulkUSBBufferedOutputStream out;

	/**
	 * Flag for whether close has been called
	 */
	private boolean closeCalled = false;

	/**
	 * Flag for whether the output stream getter has been called
	 */
	private boolean getOutputStreamCalled = false;

	/**
	 * Flag for whether the printer is ready
	 */
	private boolean isPrinterReady = false;

	/**
	 * Flag for whether the getter for printer ready has been called
	 */
	private boolean isPrinterReadyCalled = false;

	/**
	 * Flag for whether set buffer has been called
	 */
	private boolean setBufferCalled = false;

	/**
	 * Current size of buffer
	 */
	private int bufferSize = 0;

	/**
	 * Constructor for the shadow USB printer
	 * 
	 * @param context
	 */
	public void __constructor__(Context context) {
		this.context = context;
		this.out = new BulkUSBBufferedOutputStream(null, null);
	}

	/**
	 * Gets the context
	 * 
	 * @return context
	 */
	public Context getContext() {
		return this.context;
	}

	/**
	 * Shadow Implementation for EpsonUSBPrinter.close
	 */
	@Implementation
	public void close() {
		this.closeCalled = true;
	}

	/**
	 * Getter for close called
	 * 
	 * @return closeCalled
	 */
	public boolean hasCloseBeenCalled() {
		return this.closeCalled;
	}

	/**
	 * Shadow Implementation for EpsonUSBPrinter.getOutputStream
	 * 
	 * @return out
	 */
	@Implementation
	public OutputStream getOutputStream() {
		this.getOutputStreamCalled = true;
		return this.out;
	}

	/**
	 * Getter for output stream called
	 * 
	 * @return getOutputStreamCalled
	 */
	public boolean hasGetOutputStreamCalled() {
		return this.getOutputStreamCalled;
	}

	/**
	 * Shadow Implementation for EpsonUSBPrinter.isReady
	 * 
	 * @return isPrinterReadyCalled
	 */
	@Implementation
	public boolean isReady() {
		this.isPrinterReadyCalled = true;
		return this.isPrinterReady;
	}

	/**
	 * Getter for isPrinterReadyCalled called
	 * 
	 * @return isPrinterReadyCalled
	 */
	public boolean hasIsReadyBeenCalled() {
		return this.isPrinterReadyCalled;
	}

	/**
	 * Sets the flag to true
	 */
	public void setIsReady() {
		this.isPrinterReady = true;
	}

	/**
	 * Shadow Implementation for EpsonUSBPrinter.setBuffer(int)
	 * 
	 * @param bufferSize
	 */
	@Implementation
	public void setBuffer(int bufferSize) {
		this.setBufferCalled = true;
		this.bufferSize = bufferSize;
	}

	/**
	 * Getter for the current buffer size
	 * 
	 * @return bufferSize
	 */
	public int getCurrentBufferSize() {
		return this.bufferSize;
	}

	/**
	 * Getter for set buffer called
	 * 
	 * @return setBufferCalled
	 */
	public boolean hasSetBufferBeenCalled() {
		return this.setBufferCalled;
	}
}
