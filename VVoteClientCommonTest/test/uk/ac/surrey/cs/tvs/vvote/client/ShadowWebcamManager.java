package uk.ac.surrey.cs.tvs.vvote.client;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;
import org.robolectric.shadows.ShadowService;

import com.ford.openxc.webcam.WebcamManager;

/**
 * Shadow Implementation for <code>WebcamManager</code>
 */
@Implements(WebcamManager.class)
public class ShadowWebcamManager extends ShadowService {

	/**
	 * Flag for whether onCreate has been called
	 */
	private boolean isCreatedCalled = false;

	/**
	 * Overrides onCreate
	 */
	@Implementation
	public void onCreate() {
		this.isCreatedCalled = true;
	}

	/**
	 * Getter for has created been called
	 * 
	 * @return isCreatedCalled
	 */
	public boolean hasCreatedBeenCalled() {
		return this.isCreatedCalled;
	}
}
